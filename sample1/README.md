# sample script for json

* sample1.json : 코드와 나라 정보
* sample2.json : 코드와 수도 정보
* result_sample.py : 두 sample파일을 읽어 하나의 json으로 정리하여 저장
* result_sample.json : result_sample.py의 결과물
```json
{
  "result": [
    {
      "country": "Korea",
      "data": {
        "key": "AA",
        "Capital": "Seoul"
      }
    },
    {
      "country": "Japan",
      "data": {
        "key": "BB",
        "Capital": "Tokyo"
      }
    },
    {
      "country": "China",
      "data": {
        "key": "CC",
        "Capital": "Beijing"
      }
    },
    {
      "country": "Taiwan",
      "data": {
        "key": "DD",
        "Capital": "Taipei"
      }
    },
    {
      "country": "Thailand",
      "data": {
        "key": "EE",
        "Capital": "Bangkok"
      }
    }
  ]
}
```
