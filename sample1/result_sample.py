import json

# 나라 추출을 위한 펑션
def check_nation(file, key):
    with open(file) as f:
        json_object = json.load(f)
    a = json_object.get(key)
    return a

# 수도 추출을 위한 펑션
def check_cap(file, key):
    with open(file) as f:
        json_object = json.load(f)
    a = json_object.get(key)
    return a

def main(file):
    with open(file) as f:
        json_object = json.load(f)
    
    # key리스트 추출
    keys = [key for key in json_object]
    data = {}
    data['result'] = []
    # key 기준으로 나라와 수도를 추출하여 하나의 json데이터로 등록(반복)
    for key in keys:
        nation = check_nation(file, key)
        cap = check_cap("sample2.json", key)
        data['result'].append({
            "country": nation,
            "data" : {
                "key": key,
                "Capital": cap
            }
        })
    # 파일 저장
    with open("result_sample.json", 'w') as outfile:
        json.dump(data, outfile, indent=2)

#    print (data)
	

if __name__ == "__main__":
    main("sample1.json")
