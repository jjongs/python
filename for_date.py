from datetime import datetime
import pandas

start = '01-27'
end = '02-05'

start = datetime.strptime(start, "%m-%d")
end = datetime.strptime(end, "%m-%d")
dates = [date.strftime("%m-%d") for date in pandas.date_range(start, periods=(end - start).days + 1)]

print (dates)