import os 

# 디렉토리 full path뽑기 
def search_dir(dirname): 
    dirs = []
    # 하위 디렉토리 추출 
    root_dir = os.listdir(dirname) 
    for tmp in root_dir: 
        full_path = os.path.join(dirname, tmp) 
        if os.path.isdir(full_path): 
            dirs.append(full_path) 
    return dirs 

# 디렉토리 길이 추출 및 정렬
def number(path): 
    result_one = []
    dirs=search_dir(path) 
    for path in dirs: 
        lenth = len(path) 
        line_one = (lenth, path) 
        result_one.append(line_one) 
    result_one.sort(key = lambda x:x[0],reverse=True) 
    return result_one 

def main(path): 
    list = number(path) 
    for line in list: 
        print (line) 


if __name__ == "__main__": 
    main("/var")