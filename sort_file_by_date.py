from datetime import datetime

list = [
"file.abc.2022-09-05T.01-01.text.txt",
"file.abc.2022-05-06T.04-08.text.txt",
"file.abc.2022-09-05T.02-08.text.txt",
"file.abc.2022-03-01T.01-15.text.txt",
"file.abc.2022-03-01T.01-11.text.txt",
"file.abc.2022-05-06T.01-08.text.txt"
]

def parse_item(file):
    parts = file.split('.')
    date_sort = parts[2] + parts[3]
    date_time = datetime.strptime(date_sort, "%Y-%m-%dT%H-%M")
    return {
        "filename": file,
        "date": datetime.strftime(date_time, "%Y-%m-%dT%H-%M")
    }

mapping_files = [parse_item(file) for file in list]
sorted_files = sorted(mapping_files, key=lambda x: x["date"])


for sorted_file in sorted_files:
    print (sorted_file['filename'])