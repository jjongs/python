import json

json_data = [
    {"fruit": "Apple", "size": "Large", "color": "Red"},
    {"name": "User_name1", "Hobby": "Baseball"},
    {"fruit": "Orange", "size": "Medium", "color": "Yellow"},
    {"menu": "Coffee", "Type": "iced"},
    {"fruit": "Strawberry", "size": "Small", "color": "Red"}
]

for line in json_data:
    json_line = json.dumps(line)
    data = json.loads(json_line)
    color_value = data.get('color', 'None')

    if "Red" in color_value:
        print (f"{data['fruit']}\'s color is Red.")
